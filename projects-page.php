<?php

// début de page html
include("./inc/head.php");

// le <header> et la <nav
include("./inc/header.php");

// la <main id="home">, la gallerie d'image
include("./inc/projects_list.php");

// fin de page html
include("./inc/foot.php");