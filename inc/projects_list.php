<?php

$projects_children = $page->children();
$category_pages = $pages->find("template=category,sort=sort");

// data-project=
?>

<main id="projects_list">
	<div class="container_list">

	<div id="legendetel">
<?php foreach($category_pages as $category): ?>
	<?= substr($category->name, 0, 1)?> &#8193; <?= $category->title ?> <br>
<?php endforeach ?>
	</div>

	<div class="row" id="code">
		<div><p>Code</p></div>
		<div><p>Description</p></div>
		<div><p>Team</p></div>
	</div>


<?php 


/*
Array (
    [0] => Array (
        [0] => advertising
        [1] => 5
	)
*/
// creation du tableau des tableaux des categories et du nombre de pages
$array = array();
foreach($category_pages as $category) {
	$count = $pages->find("categories*=".$category->name."")->count + 1;
	$name_and_count = array($category->name, $count);
	array_push($array, $name_and_count);
}

// fonction qui retourner la key du projet dans le tableau général
function getKey($browsed_array, $child_name) {
foreach($browsed_array as $key=>$value){
    if ($value[0] == $child_name) return $key;
}
}

?>


<?php foreach($projects_children as $key=>$project_child): ?>
<?php
$key_in_array = getKey($array, $project_child->categories->name);
$array[$key_in_array][1]--;
$code = $array[$key_in_array][1];
while (strlen($code) < 3) {
	$code = "0".$code;
}
?>
		<a data-project="<?=$project_child->name?>" data-href="<?=$project_child->url?>">
			<div class="row">
				<div>
					<p><?=substr($project_child->categories->title, 0, 1)?><?= $code ?></p>
				</div>
				<div>
					<?=$project_child->project_description?> 
				</div>
				<div>
					<?=$project_child->team?> 
				</div>
			</div>
		</a>
		<hr>
<?php endforeach ?>
	</div>
</main>


<aside id="aside_projects_list">
<?php foreach($projects_children as $project_child): ?>
<?php $small_img_miniature = $project_child->img_miniature->width("350px") ?>
	<figure id="<?=$project_child->name?>">
		<img data-src="<?=$small_img_miniature->url?>">
	</figure>
<?php endforeach ?>
</aside>

<div id="legende">
<?php foreach($category_pages as $category): ?>
	<?= substr($category->name, 0, 1)?> &#8193; <?= $category->title ?> <br>
<?php endforeach ?>
</div>
