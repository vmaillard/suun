<?php 

$homepage_children = $homepage->children("template!=empty-page");

?>

<?php if($page->id == $homepage->id || $page->template->name == 'about-page'): ?>
<header class="background">
<?php else: ?>
<header>
<?php endif ?>
	<nav>
		<h1><a data-href="<?=$homepage->url?>"><?=$homepage->title?></a></h1>
<?php foreach($homepage_children as $homepage_child): ?>
<?php if($page->id == $homepage->id): ?>
		<h2 id="<?=$homepage_child->name?>"><a data-href="<?=$homepage_child->url?>"><?=$homepage_child->title?></a><span></span></h2>
<?php elseif ($page->id == $homepage_child->id): ?>
		<h2><a class="hidden_device_tel" data-href="<?=$homepage_child->url?>"><?=$homepage_child->title?></a></h2>
<?php else: ?>
		<h2><a class="hidden" data-href="<?=$homepage_child->url?>"><?=$homepage_child->title?></a></h2>
<?php endif ?>
<?php endforeach ?>
<?php if($page->template->name == 'home'): ?>
		<a href="mailto:sayhi@suunconsultancy.com">Say hi</a>
<?php else: ?>
		<a class="hidden" href="mailto:sayhi@suunconsultancy.com">Say hi</a>
<?php endif ?>
	</nav>
</header>
