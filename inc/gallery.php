<?php

$projects_page = $pages->get('/projects');
$projects_children = $projects_page->children();

?>

<main id="home">
<?php foreach($projects_children as $project_child): ?>
<?php foreach($project_child->imgs_home as $image): ?>
<?php 
$small_image1 = $image->height("210px");
$small_image2 = $image->height("150px");
$small_image3 = $image->height("100px");
$small_image4 = $image->height("50px");

$srcset = $small_image1->url;
$sizes = '(max-width: 500px) 500px, (max-width: 800px) 800px, (max-width: 1000px) 1000px, (max-width: 1500px) 1500px, 2000px';

if ($image->layout == "small") {
	$srcset = $small_image4->url;
	$sizes = '50px';
} else if ($image->layout == "normal") {
	$srcset = $small_image3->url;
	$sizes = '100px';
} else if ($image->layout == "medium") {
	$srcset = $small_image3->url .' 100w, '. $small_image2->url .' 150w, '. $small_image1->url .' 210w';
	$sizes = '(max-width: 1024px) 100px, (max-width: 2560px) 150px, 210px';
} else if ($image->layout == "big") {
	$srcset = 	$small_image3->url .' 100w, '. $small_image2->url .' 150w, '. $small_image1->url .' 210w';
	$sizes = '(max-width: 480px) 100px, (max-width: 1300px) 150px, 210px';
}


?>

		<figure data-title="<?= $project_child->title ?>">
			<a class="<?=$project_child->name?> <?=$image->layout?>" data-project="<?=$project_child->name?>" data-href="<?=$project_child->url?>">
				<img class="lazyload home_img" src="<?=$small_image1->url?>" data-srcset="<?=$srcset?>" sizes="<?=$sizes?>">
			</a>
		</figure>
<?php endforeach ?>
<?php if($project_child != $projects_children->last()): ?>
		<figure class="flex"></figure>
<?php endif ?>
<?php endforeach ?>
</main>

