<?php
// data-project=

?>

<main id="projects_list">
	<ul>
<?php foreach($page->clients as $client): ?>
		<li>
			<a data-project="<?=$sanitizer->name($client->client_name)?>" href="<?=$client->client_url?>">
			<?=$client->client_name?> 
			</a>
		</li>
<?php endforeach ?>
	</ul>
</main>



<aside id="aside_clients_list">
<?php foreach($page->clients as $client): ?>
	<figure id="<?=$sanitizer->name($client->client_name)?>">
		<img src="<?=$client->image->url?>" alt="<?=$client->$image->description?>">
	</figure>
<?php endforeach ?>
</aside>