document.addEventListener("DOMContentLoaded", function() {
	init();
}, false);


function init() {
	
	/* GÉNÉRAL ------------------- */

	ScreenSize();
	registerListener('resize', ScreenSize);

	// event a data-href
	var a_tags = document.querySelectorAll('a[data-href]');
	a_tags.forEach(function (item) {
		item.addEventListener('click', change_location, false);
	});


	/* HOME ------------------- */
	
	// scroll event sur tel sur /home
	if (isMobile.matches == true || isMobileLandscape.matches == true) {
		registerListenerQuick('scroll', highlight_scroll2);

		/*
		set_highlight_scroll();
		highlight_scroll();
		registerListener('scroll', highlight_scroll);
		registerListener('resize', highlight_scroll);
		*/
	}

	var click = false;
	// event scrollto sur logo
	$("#logo").click(function(){
		click = true;
		$("html, body").animate({
			scrollTop: $('#home').offset().top-42
		}, 700, 'easeOutCubic');
	});

	var logo = document.querySelector("#logo");
	if (logo) {
	setTimeout(function(){
		if (!click) {
			$("html, body").animate({
				scrollTop: $('#home').offset().top-42
			}, 700, 'easeOutCubic');
		} else {
			console.log("hlo");
		}
	}, 1500);
	}
	

	var imgs_gallery = document.querySelectorAll("#home figure a");
	imgs_gallery.forEach(function (item) {
		item.addEventListener('mouseenter', toggleColorAndTitle, false);
		item.addEventListener('mouseleave', toggleColorAndTitle, false);
	}); 


	/* BASIC_PAGE  ------------------- */
	var slider = document.querySelector("#slider");
	if (slider) setup_slider();
	

	/* PROJECTS  ------------------- */

	// boucle à travers les miniatures
	setTimeout(function(){
		compteur = 0;
		loopMiniaturesProjets();
	}, 400);
	
	// preload des trois premières miniatures
	preloadMiniatures();

	// event sur liste
	var li_projects_list = document.querySelectorAll("#projects_list a");
	li_projects_list.forEach(function (item) {
		item.addEventListener('mouseenter', toggle_show, false);
		item.addEventListener('mouseleave', toggle_hide, false);
  	});


	/* ABOUT  ------------------- */

	// event sur l'étoile
	var etoile = document.querySelector("#etoile a");
	if (etoile) etoile.addEventListener('click', eventOnStar, false);
}

function ScreenSize() {
    var w = window,
    d = document,
    e = d.documentElement,
    g = d.getElementsByTagName('body')[0];

    var w2 = window,
    d2 = document,
    e2 = d2.documentElement,
    g2 = d2.getElementsByTagName('body')[0];
    
    screen_height = w2.innerHeight|| e2.clientHeight|| g2.clientHeight;
	screen_width = w2.innerWidth|| e2.clientWidth|| g2.clientWidth;
	
	// detect if mobile / tablet
	isMobile = window.matchMedia("only screen and (max-width: 768px) and (max-device-width : 768px)");
	isMobileLandscape = window.matchMedia("only screen and (max-device-width : 1030px) and (orientation : landscape)");

	// detect touch devices
	window.addEventListener('touchstart', function onFirstTouch() {
		window.USER_IS_TOUCHING = true;
		window.removeEventListener('touchstart', onFirstTouch, false);
	}, false);
}

function change_location() {
	// window.location.replace(this.getAttribute('data-href'));
	// sur la page projet, on ne veut pas target = "_blank"
	var target = this.getAttribute('target');
	if (target == null) {
		target = "_self";
	}
	// console.log(target);
	// on chope l'attribut data-href, et on change l'url
	window.open(this.getAttribute('data-href'), target);
}

function eventOnStar(event) {
	var img = this.querySelector("img");
	if (img) {
		var span = document.createElement("SPAN");
		span.innerHTML = "hips don't lie";
		this.innerHTML = "";
		this.appendChild(span);
	} else {
		event.preventDefault();
	}
}



















/*


console.log();

document.createElement();
document.createTextNode();


var video = document.createElement('video');
video.src = 'vid/' + target + '.mp4';
video.setAttribute("controls","true");
video.setAttribute("onended","playVideoSuivante()");
video.style.height = "100px";
video.style.cssText = '';
video.className
video.id 

element.appendChild(video);
video.load();
video.play();

document.getElementById();
document.getElementsByClassName()[];
document.getElementsByTagName()[];
document.querySelectorAll("p.intro, p.suite");

var NbElement = element.childNodes.length - 1;

element.appendChild();
element.removeChild(enfant);


// vérifier si une class a des éléments :
if(class.length > 0)

// existence, type :
typeof number/text/aBoolean
// conversion vers number
parseInt(text);

// next element
.nextElementSibling

var tableau = [];
tableau.push();


for (i = 0; i < 1; i++) {
}
//condition ternaire
var = condition ? expr1 : expr2 

element.addEventListener(levent, lafonction, useCapture);
"click""mousedown""mousemove""mouseover""mouseenter""mouseleave"
element.removeEventListener(type, listener, useCapture)

array.forEach(function (item) {
	item.addEventListener('levent', lafonction, false);
});


function lafonction() {
}


element.classList.toggle();
el.classList.remove();
el.classList.add();
el.classList.contains();

el.toLowerCase().split(' ').join('-').replace();

// replace all
str = "Test abc test test abc test...".split("abc").join("");
//The general pattern is
str.split(search).join(replacement)


$(".class:eq(0)")

$('html, body').animate({
    scrollTop: el.offset().top
    }, 2000);


== égal à

!= différent de

=== contenu et type égal à

!== contenu ou type différent de

> supérieur à

>= supérieur ou égal à

< inférieur à

<= inférieur ou égal à


&& ET

|| OU

! NON

*/


