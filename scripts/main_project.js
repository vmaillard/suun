

/* EVENT sur LISTE ------------------- */

function toggle_show() {
	var data_project = this.getAttribute("data-project");
	var related_project = document.getElementById(data_project);
	var related_project_img = related_project.querySelector("IMG");

	var data_src = related_project_img.getAttribute("data-src");
	related_project_img.setAttribute("src", data_src);
	related_project.classList.add("activated");

	// on chope les hr pour les colorer
  	// console.log(this);
	this.previousElementSibling.classList.toggle("hovered");
	this.nextElementSibling.classList.toggle("hovered");
}

function toggle_hide() {
  	var data_project = this.getAttribute("data-project");
  	var related_project = document.getElementById(data_project);
	
	related_project.classList.remove("activated");

	// on chope les hr pour les colorer
	this.previousElementSibling.classList.toggle("hovered");
	this.nextElementSibling.classList.toggle("hovered");
}



/* MINIATURES ------------------- */

function loopMiniaturesProjets() {
	if (isMobile.matches) {
	  var miniatures = document.querySelectorAll("#aside_projects_list figure");
	  if (miniatures.length) {
		activatedInLoop();
		timer = setInterval(activatedInLoop, 1500);
	  }
	} else if (isMobileLandscape.matches) {
	  var miniatures = document.querySelectorAll("#aside_projects_list figure");
	  if (miniatures.length) {
		activatedInLoop();
		timer = setInterval(activatedInLoop, 1500);
	  }
	} else {
	  // console.log("hlo");
	  if( typeof timer === 'undefined' || timer === null ){
	  } else {
		clearInterval(timer);
	  }
	}
}


function activatedInLoop () {
	var miniatures = document.querySelectorAll("#aside_projects_list figure");
	var activated = document.querySelector(".activated");
	if (activated) activated.classList.remove("activated");
	
	var img_miniature = miniatures[compteur].querySelector("IMG");
	var data_src = img_miniature.getAttribute("data-src");
	if (img_miniature.hasAttribute("src") != true) {
	  img_miniature.setAttribute("src", data_src);
	  miniatures[compteur].classList.add("activated");
	}
  
	if (miniatures[compteur + 1]) {
	  var next_img_miniature = miniatures[compteur + 1].querySelector("IMG");
	  var next_data_src = next_img_miniature.getAttribute("data-src");
	  next_img_miniature.setAttribute("src", next_data_src);
	}
  
	miniatures[compteur].classList.add("activated");
  
	compteur++;
	if (compteur == miniatures.length) {
	  compteur = 0;
	}
}

function preloadMiniatures() {
	var miniatures = document.querySelectorAll("#aside_projects_list figure");
	if (miniatures.length) {
    	var compteur = 0;
      	var timer2 = setInterval(function() {
			var img_miniature = miniatures[compteur].querySelector("IMG");
			var data_src = img_miniature.getAttribute("data-src");
			if (img_miniature.hasAttribute("src") != true) {
				img_miniature.setAttribute("src", data_src);
			}
			compteur++;
			if (compteur >= 3) {
				clearInterval(timer2);
			}
		}, 200);
	}
}