
function toggleColorAndTitle(e) {
  if(window.USER_IS_TOUCHING != true) {
    var data_project = this.getAttribute("data-project");

    var data_title = this.parentElement.getAttribute("data-title");
    var h2_projects = document.getElementById("projects");
    h2_projects.classList.toggle("replaced");
    var span = h2_projects.querySelector("span");
    
    if (e.type == "mouseleave") {
      // span.innerHTML = "";
    } else {
      span.innerHTML = data_title;
    }

    var all_imgs_project = document.getElementsByClassName(data_project);
    for (var i = 0; i < all_imgs_project.length; i++) {
      all_imgs_project[i].parentElement.classList.toggle("colored");
    }
  }
}


var data_project = "";

function highlight_scroll2() {
    var this_scroll_top  = this.pageYOffset || this.scrollTop;
    var mire = document.querySelector("#mire");
    var all_figure = document.querySelectorAll('main#home figure:not(.flex)');

    if (screen_width < 600) {
        var line_height = 120;
    } else {
        var line_height = 130;
    }
    

    if (this_scroll_top > (screen_height/2 + 20)) {
    	var xPos = this_scroll_top - screen_height/2 - 20;

    	var modulo = (xPos % line_height) * (screen_width - 80) / 100;
    	mire.style.transform = "translate3d(" + modulo + "px, 0, 0)";

      for (var i = 0; i < all_figure.length; i++) {
        var rect = all_figure[i].getBoundingClientRect();

        var condition1 = (rect.left < modulo + 20) && (rect.left > modulo - all_figure[i].offsetWidth);

        if (rect.top < (screen_height/2 - 0) && rect.top > (screen_height/2) - 120 && condition1) {
			
			data_project = all_figure[i].querySelector("a").getAttribute("data-project");
            var all_imgs_project = document.getElementsByClassName(data_project);
            for (var j = 0; j < all_imgs_project.length; j++) {
                all_imgs_project[j].classList.add("active");
			}

		} else if (data_project.length) {
			
			var cond1 = all_figure[i].querySelector("a").classList.contains("active");
			var cond2 = !all_figure[i].querySelector("a").classList.contains(data_project);
			if (cond1 && cond2) {
				all_figure[i].querySelector("a").classList.remove("active");
			}
		
		}
          
      }
    } else if (document.querySelector('.active') != undefined) {
      var active = document.querySelectorAll('.active');
      for (var i = 0; i < active.length; i++) {
        active[i].classList.remove("active");
      }
    }
}

var lazy = [];
function set_highlight_scroll(){
	lazy = document.querySelectorAll("#home figure:not(.flex)");
    console.log('Found ' + lazy.length + ' lazy images');
} 
var pos1 = 0;
function highlight_scroll(){
	
    for(var i=0; i<lazy.length; i++){
        var scroll_top  = window.pageYOffset || document.documentElement.scrollTop;
        
        if (screen_width < 600) {
            var division = 40;
            // 30
        } else {
            var division = 18;
            // 20
        }
             
        if(scroll_top > pos1) {
        // console.log("down");
        var debug_scroll = 0;
        }
        if(pos1 > scroll_top) {
        var debug_scroll = 100;
        // console.log("up");
        }
        pos1 = scroll_top;

        scroll_percent = Math.round((scroll_top - (screen_height / 2) - debug_scroll) / division);
        
        if (i == scroll_percent) {
            var data_project = lazy[i].childNodes[1].getAttribute("data-project");
        }

        if (scroll_percent >= 0 && i == scroll_percent) {
            var all_imgs_project = document.getElementsByClassName(data_project);
            // console.log(all_imgs_project);
            // lazy[i].classList.contains("active")) {
                
            
            for (var j = 0; j < all_imgs_project.length; j++) {
                // console.log(all_imgs_project[j]);
                all_imgs_project[j].classList.add("active");
            }
            
        } else if (scroll_percent >= 0) {
            if (lazy[i].childNodes[1].classList.contains(data_project) != true) {
                lazy[i].childNodes[1].classList.remove("active");
            }
        }
        
    }

}


function cleanLazy(){
    lazy = Array.prototype.filter.call(lazy, function(l){ return l.getAttribute('data-scrolled');});
}

// middle
function isInViewportMiddle(el){
	var rect = el.getBoundingClientRect();

    return (
        rect.bottom >= (((window.innerHeight - 50) /2) || document.documentElement.clientHeight) && 
        rect.right >= 0 && 
        rect.top <= ((window.innerHeight /2) || document.documentElement.clientHeight) && 
        rect.left <= (window.innerWidth || document.documentElement.clientWidth)
     );
}


// top
function isInViewportTop(el){
    var rect = el.getBoundingClientRect();
    
    return (
        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && 
        rect.right >= 0 && 
        rect.top <= 0 && 
        rect.left <= (window.innerWidth || document.documentElement.clientWidth)
     );
}

// bottom
function isInViewportBottom(el){
    var rect = el.getBoundingClientRect();
    
    return (
        rect.bottom >= 0 && 
        rect.right >= 0 && 
        rect.top <= (window.innerHeight || document.documentElement.clientHeight) && 
        rect.left <= (window.innerWidth || document.documentElement.clientWidth)
     );
}

function registerListenerQuick(event, func) {
    if (window.addEventListener) {
        window.addEventListener(event, throttle(func, 10));
    } else {
        window.attachEvent('on' + event, func)
    }
}
function registerListener(event, func) {
    if (window.addEventListener) {
        window.addEventListener(event, throttle(func, 100));
    } else {
        window.attachEvent('on' + event, func)
    }
}

// fonction pour alléger l'event scroll et resize
var throttle = function(func, wait) {

    var context, args, timeout, throttling, more, result;
    return function() {
      context = this; args = arguments;
      var later = function() {
        timeout = null;
        if (more) func.apply(context, args);
      };
      if (!timeout) timeout = setTimeout(later, wait);
      if (throttling) {
        more = true;
      } else {
        result = func.apply(context, args);
      }
      throttling = true;
      return result;
    };
};

