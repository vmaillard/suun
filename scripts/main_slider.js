// $(document).ready(function(){

function setup_slider () {

	var prev_project = document.getElementById("prev_project");
	var next_project = document.getElementById("next_project");
	prev_project.addEventListener("click", go_to_prev_project);
	next_project.addEventListener("click", go_to_next_project);

	var isMobile = window.matchMedia("only screen and (max-width: 768px) and (max-device-width : 768px)");
	var isMobileLandscape = window.matchMedia("only screen and (max-device-width : 1030px) and (orientation : landscape)");
	if (isMobile.matches != true && isMobileLandscape.matches != true) {
		var body = document.getElementsByTagName("body")[0];
		body.addEventListener("mousemove", track_mouse);
		body.addEventListener("click", get_mouse_position);			
	} else {
		// sur mobile
		// https://github.com/gajus/orientationchangeend
		var config = {},
		OCE;
		OCE = gajus.orientationchangeend(config);
		OCE.on('orientationchangeend', function () {
			// The orientation have changed.
			center_height();
		});

		var body = document.getElementsByTagName("body")[0];
		body.addEventListener('touchstart', get_touch_position);
    }

	// on récupère le numéro du slide après le # de l'url
	var hash = parseInt(location.hash.substr(1));
	// si il n'y a pas de #
	if (isNaN(hash)) hash = 0;

	$('#slider').on('init', function(event, slick){	
		// slider intialisé
		first_slide();
		// on retouche le placement central les slides sur mobile (pb de la height url)
		center_height();
		// démarrer l'autoplay de la vidéo du slide
    	play_vid();
	});

	$('#slider').slick({
		initialSlide: hash,
		edgeFriction: 0.1,
		speed: 200,
		// lazyLoad: 'ondemand',
		lazyLoad: 'progressive',
  		fade: true,
		infinite: false,
		slidesToShow: 1,
		rows: 0,
		prevArrow:"<button type='button' id='slick-prev'></button>",
		nextArrow:"<button type='button' id='slick-next'></button>",
	});

	$('#slider')
        .on('beforeChange', function(event, slick, currentSlide, nextSlide) {
        	removeHash();
        	// slider avant slide
        	first_slide();
            normal_slide();

            // pause de la vidéo en cours
			pause_vid();
			
			// debug pour changer le curseur
			if (isMobile.matches != true && isMobileLandscape.matches != true) {
				check_mouse();
			} else {
			  // sur mobile
		  	}
        })
        .on('afterChange', function(event, slick, currentSlide, nextSlide) {
        	// slider après slide
        	first_slide();

        	// démarrer l'autoplay de la vidéo du slide
        	play_vid();
        	
          	if (slick.$slides.length-1 == currentSlide) {
			    last_slide();
		  	}		  	
    });

    // si jamais l'url dirige vers le dernier slide
    // alors on fait apparaître NEXT et PREVIOUS
    if (hash == ($('.slick-slide').length - 1)) {
		last_slide();
	}

    // si jamais il y a une seule image
    // alors on fait apparaître NEXT et PREVIOUS
    if ($('.slick-slide').length == 1) {
		last_slide();
	}
}

function go_to_next_project() {
	var next_project = document.getElementById("next_project");
	window.open(next_project.getAttribute('data-href'), "_self");
}

function go_to_prev_project() {
	var prev_project = document.getElementById("prev_project");
	window.open(prev_project.getAttribute('data-href'), "_self");
}

function go_to_prev_slide() {
	$('#slider').slick('slickPrev');
}

function go_to_next_slide() {
	$('#slider').slick('slickNext');
}

function first_slide() {
	var first_child = $(".slick-slide:first-child");
	var current_slide = $('.slick-current');
	if (first_child.is(current_slide)) {
		var prev_project = document.getElementById("prev_project");
		prev_project.classList.add("activated");
	}
}

function last_slide() {	
	var next_project = document.getElementById("next_project");
	next_project.classList.add("activated");

	var slick_active = document.getElementsByClassName("slick-active")[0];
	slick_active.classList.add("activated");
}

function normal_slide() {
	var slick_active = document.getElementsByClassName("slick-slide activated")[0];
	if (slick_active) {
	slick_active.classList.remove("activated");
	}
	var links_slider_active = document.querySelectorAll(".project_arrow.activated")[0];
	//console.log(links_slider_active);
	if (links_slider_active) {
	links_slider_active.classList.remove("activated");
	}
}


function play_vid() {
	var video = document.querySelectorAll('.slick-current video')[0];
	if (video) video.play();
}

function pause_vid() {
	var video = document.querySelectorAll('.slick-current video')[0];
	if (video) video.pause();
}

function center_height() {
	// pour centrer pile poil en mobile
	var isMobile = window.matchMedia("only screen and (max-width: 768px) and (max-device-width : 768px)");
	var isMobileLandscape = window.matchMedia("only screen and (orientation : landscape)");

	if (isMobile.matches || isMobileLandscape.matches) {
	
		// PORTRAIT mode
		var w = window,
	    d = document,
	    e = d.documentElement,
	    g = d.getElementsByTagName('body')[0],
	    y = w.innerHeight|| e.clientHeight|| g.clientHeight;
		
	    var slick_slide = document.querySelectorAll('.slick-slide');
	    slick_slide.forEach(function (item) {
	    	
	    	item.style.height = y + "px";
	    });
	    var project_arrow = document.querySelectorAll('.project_arrow');
	    project_arrow.forEach(function (item) {
	    	item.style.height = y + "px";
		});	

	}
}

function removeHash() { 
    history.pushState("", document.title, window.location.pathname + window.location.search);
}

function track_mouse(event) {	
	// si la souris est à gauche
	if (event.pageX < (window.innerWidth / 2) && event.pageY > 42) {
		var slider = document.getElementById("slider");
		slider.style.cursor = "w-resize";
	// si la souris est à droite
	} else if (event.pageX > (window.innerWidth - (window.innerWidth / 2)) && event.pageY > 42) {
		var slider = document.getElementById("slider");	
		slider.style.cursor = "e-resize";
	// si la souris est ailleurs
	} else {

	}
}

function check_mouse() {
	// si la souris est à gauche
	if (mouseX < (window.innerWidth / 2) && mouseY > 42) {
		var slider = document.getElementById("slider");
		slider.style.cursor = "w-resize";
	// si la souris est à droite
	} else if (mouseX > (window.innerWidth - (window.innerWidth / 2)) && mouseY > 42) {
		var slider = document.getElementById("slider");	
		slider.style.cursor = "e-resize";
	// si la souris est ailleurs
	} else {

	}
}

function get_mouse_position (event) {
	mouseX = event.clientX;
	mouseY = event.clientY;
	if (mouseX < (window.innerWidth / 2) && mouseY > 42) {
		go_to_prev_slide();
	} else if (mouseX > (window.innerWidth - (window.innerWidth / 2)) && mouseY > 42) {
		go_to_next_slide();
	} else {

	}
}

function get_touch_position(e) {
	// e.targetTouches[0].pageX
	// si touch à gauche
	if (e.targetTouches[0].pageX < (window.innerWidth / 2) && e.targetTouches[0].pageY > 42) {
		go_to_prev_slide();
	// si touch à droite
	} else if (e.targetTouches[0].pageX > (window.innerWidth - (window.innerWidth / 2)) && e.targetTouches[0].pageY > 42) {
		go_to_next_slide();
	}
}