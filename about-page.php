<?php

// début de page html
include("./inc/head.php");

// le <header> et la <nav
include("./inc/header.php");
?>


<div id="tiret">
	<img src="<?=$config->urls->assets?>files/logo/logo-about0.png">
</div>

<main id="about">

	<div id="sun">
		<img src="<?=$config->urls->assets?>files/logo/logo-about1.png">
	</div>

	<div id="suun">
		<img src="<?=$config->urls->assets?>files/logo/logo.jpg">
	</div>

	<div id="principal">
		<?=$page->bio?>
	</div>

	<div id="contacts">
		<?=$page->contact?>
	</div>

	<div id="u">
		<img src="<?=$config->urls->assets?>files/logo/logo-about2.jpg">
	</div>

	<div id="clients">
		<p>
			Thank you<br><br>
			<?=$page->clients?>
		</p>	
	</div>


	<div id="etoile">
		<a target="_blank" href="https://www.youtube.com/watch?v=DUT5rEU6pqM">
			<img src="<?=$config->urls->assets?>files/logo/suun.png">
		</a>
	</div>

	<div id="credits">
		<p>
			Design by <a data-href="http://baptistegb.fr/" target="_blank">Baptiste Gerbelot Barillon</a>
			<span class="sautligne"><br></span>
			<span class="espace"> &#8193; </span>
			Coding by <a data-href="http://alexiscros.com/" target="_blank">Alexis Cros</a> &amp; <a data-href="http://vincent-maillard.fr/" target="_blank">Vincent Maillard</a>
			<span class="sautligne"><br></span>
			<span class="espace"> &#8193; </span>
			Logo by <a data-href="http://www.choquelegoff.com/" target="_blank">Atelier Choque Le Goff</a>
		</p>
	</div>

</main>
<?php
// fin de page html
include("./inc/foot.php");
?>