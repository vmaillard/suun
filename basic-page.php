<?php
include("./inc/head.php");
include("./inc/header.php");

$slider = $page->slider;

$projects_page = $pages->get('/projects');
$projects_children = $projects_page->children();

$prev_page = $page->prev->id ? $page->prev : $page->siblings->last();
$next_page = $page->next->id ? $page->next : $page->siblings->first();

$last_slide = $prev_page->slider->count;
if($prev_page->slide_title != true) {
	$last_slide = $last_slide - 1;
}
?>


<a id="croix" data-href="<?=$projects_page->url?>">×</a>


<main id="slider">
<?php if($page->slide_title): ?>
	<div class="slide_title">
	<?php echo $page->slide_title; ?>
	</div>
<?php endif ?>
<?php foreach($slider as $slide): ?>

<?php if($slide->video): ?>
<?php 
if ($slide->image) {
	$options = array('upscaling' => false);
	$poster = $slide->image->width("2000px", $options);
} else {
	$poster->url = "";
}
?>
	<div>
	<video class="<?=$slide->slide_layout->title?>" poster="<?=$poster->url?>" muted autoplay loop preload="metadata">
	  	<source src="<?= $slide->video->url ?>" type="video/mp4">
	  	Your browser does not support the video tag.
	</video>
	</div>
<?php else: ?>
<?php 
$options = array('upscaling' => false);
$image1 = $slide->image->width("500px", $options);
$image15 = $slide->image->width("800px", $options);
$image2 = $slide->image->width("1000px", $options);
$image3 = $slide->image->width("1500px", $options);
$image4 = $slide->image->width("2000px", $options);
// $image5 = $slide->image->width("2500px");
$sizes = '(max-width: 500px) 500px, (max-width: 800px) 800px, (max-width: 1000px) 1000px, (max-width: 1500px) 1500px, 2000px';
if ($slide->slide_layout->title == "slide_big") {
	$sizes = '(max-width: 500px) 500px, (max-width: 800px) 800px, (max-width: 1000px) 1000px, (max-width: 1500px) 1500px, 2000px';
} else if ($slide->slide_layout->title == "slide_normal") {
	$sizes = '(max-width: 750px) 500px, (max-width: 1200px) 800px, (max-width: 1500px) 1000px, (max-width: 2200px) 1500px, 2000px';
} else if ($slide->slide_layout->title == "slide_small") {
	$sizes = '(max-width: 1100px) 500px, (max-width: 1750px) 800px, (max-width: 2200px) 1000px, 1500px';
}
?>
	<figure>
		<img class="slide <?=$slide->slide_layout->title?>" src="<?=$slide->image->url?>" alt="<?=$image->description?>" srcset="<?=$image1->url?> 500w, <?=$image15->url?> 800w, <?=$image2->url?> 1000w, <?=$image3->url?> 1500w, <?=$image4->url?> 2000w" sizes="<?=$sizes?>">
	</figure>
<?php endif ?>


<?php endforeach ?>
</main>



<div class="slider_arrow" id="prev_slide"></div>
<div class="slider_arrow" id="next_slide"></div>

<div class="project_arrow" id="prev_project" data-href="<?=$prev_page->url?>#<?=$last_slide?>"></span></div>
<div class="project_arrow" id="next_project" data-href="<?=$next_page->url?>"></div>


<?php include("./inc/foot.php"); ?>




